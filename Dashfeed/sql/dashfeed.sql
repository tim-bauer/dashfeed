-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 16, 2015 at 12:59 PM
-- Server version: 5.5.41
-- PHP Version: 5.5.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dashfeed`
--

-- --------------------------------------------------------

--
-- Table structure for table `USER`
--

CREATE TABLE IF NOT EXISTS `USER` (
`ID_USER` int(10) NOT NULL,
  `EMAIL` varchar(30) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `ADMIN` tinyint(1) NOT NULL,
  `CONFIRMLINK` varchar(50) NOT NULL,
  `CONFIRMED` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `USER_WIDGET`
--

CREATE TABLE IF NOT EXISTS `USER_WIDGET` (
`ID` int(5) NOT NULL,
  `USER_ID` int(10) NOT NULL,
  `WIDGET_ID` int(10) NOT NULL,
  `COLOUR` int(11) NOT NULL,
  `X` int(11) NOT NULL,
  `Y` int(11) NOT NULL,
  `HEIGHT` int(10) NOT NULL,
  `COLLAPSED` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `WIDGETS`
--

CREATE TABLE IF NOT EXISTS `WIDGETS` (
`ID` int(10) NOT NULL,
  `HEIGHT` int(10) NOT NULL,
  `WIDTH` int(10) NOT NULL,
  `RSS_FEED` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `USER`
--
ALTER TABLE `USER`
 ADD PRIMARY KEY (`ID_USER`);

--
-- Indexes for table `USER_WIDGET`
--
ALTER TABLE `USER_WIDGET`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `WIDGETS`
--
ALTER TABLE `WIDGETS`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `USER`
--
ALTER TABLE `USER`
MODIFY `ID_USER` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `USER_WIDGET`
--
ALTER TABLE `USER_WIDGET`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `WIDGETS`
--
ALTER TABLE `WIDGETS`
MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
