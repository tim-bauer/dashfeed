<?php
	//Session starten bzw. auf vorhandene Session zugreifen	
	session_start();
	$id = session_id();

	//Userdaten holen, wenn er eingeloggt ist
	$loggedIn = false;
	if(isset($_SESSION["loggedIn"])) {
		$loggedIn = $_SESSION["loggedIn"];
		$username = $_SESSION["username"];
	}
?>
<html>
	<head>
		<title>Dashfeed</title>
		<!-- Einbinden der Style-Datei -->
		<link rel="stylesheet" href="css/index.css" type="text/css">
		<link rel="stylesheet" href="css/jquery.gridster.css" type="text/css">
		<link rel="stylesheet" href="css/jquery.gridster.min.css" type="text/css">
		<script type="text/javascript" src ="js/jquery.js"></script>
		<script type="text/javascript" src ="js/jquery-ui.js"></script>
		<script type="text/javascript" src ="js/jquery-ui.min.js"></script>
		<script type="text/javascript" src ="js/custom.js"></script>
		<script type="text/javascript" src ="js/utils.js"></script>
		<script type="text/javascript" src ="js/jquery.collision.js"></script>
		<script type="text/javascript" src ="js/jquery.coords.js"></script>
		<script type="text/javascript" src ="js/jquery.draggable.js"></script>
		<script type="text/javascript" src ="js/jquery.gridster.min.js"></script>
		<script type="text/javascript" src ="js/jquery.gridster.js"></script>
	</head>
	<header>
	<!-- Navigationsleiste mit Logo -->
	<div id=header>
		<div id=nav>
			<ul>
				<li><a id=faq href="">FAQ</a></li>
				<?php 
					if($loggedIn) 
						echo '<li><a id=login href="">Hallo $username!</a></li>';
					else {
						echo '<li><a href="#" class="registerpopup">Registrieren</a></li>';
						echo '<li><a id=login href="">Anmelden</a></li>';
					}
				?>
			</ul>
		</div>
		<div id=logo>
			<img width=250 height=100 src="images/index/logo.jpg"></img>		
		</div>
	</div>
<<<<<<< HEAD
	</header>
	<body>
		<?php include '/Login/register.php';?>
		<!-- Hauptbereich mit Men� und Widgets -->
		<div id=main>
			<div id=menu>
				<div id=addWidget><input type="button" onClick="addWidget()"/>Widget hinzuf�gen</div>
			</div>
			<div id=content>
				<div class="gridster">
				   	<ul class="no-bullets">
				        <li class="gs-w draggable" data-row="1" data-col="1" data-sizex="1" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				        <li class="gs-w draggable" data-row="2" data-col="1" data-sizex="1" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				        <li class="gs-w draggable" data-row="3" data-col="1" data-sizex="1" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				 
				        <li class="gs-w draggable" data-row="1" data-col="2" data-sizex="2" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				        <li class="gs-w draggable" data-row="2" data-col="2" data-sizex="2" data-sizey="2">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				 
				        <li class="gs-w draggable" data-row="1" data-col="4" data-sizex="1" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				        <li class="gs-w draggable" data-row="2" data-col="4" data-sizex="2" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				        <li class="gs-w draggable" data-row="3" data-col="4" data-sizex="1" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				 
				        <li class="gs-w draggable" data-row="1" data-col="5" data-sizex="1" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				        <li class="gs-w draggable" data-row="3" data-col="5" data-sizex="1" data-sizey="1">
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				 
				        <li class="gs-w draggable" data-row="1" data-col="6" data-sizex="1" data-sizey="1" >
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				        <li class="gs-w draggable" data-row="2" data-col="6" data-sizex="1" data-sizey="2" >
				        	<div class="crossClose"></div>
				        	<span class="gs-resize-handle gs-resize-handle-both"></span>
				        </li>
				    </ul>
				</div>
			</div>
			<!-- Untere Leiste mit Copyright und Kontakt -->
			<div id=footer>
				<ul>
					<li>Copyright dashfeed &copy; 2015</li>
					<li>-</li>
					<li><a id=contact href="">Kontakt</a></li>
					<li>-</li>
					<li><a id=impressum href="">Impressum</a></li>
					<li>-</li>
					<li><a id=project href="">Projekthintergrund</a></li>
				</ul>
=======
	<script type="text/javascript" src ="js/jquery.js"></script>
	<script type="text/javascript" src ="js/jquery-ui.js"></script>
	<script type="text/javascript" src ="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src ="js/custom.js"></script>
	<script type="text/javascript" src ="js/utils.js"></script>
	<script type="text/javascript" src ="js/jquery.collision.js"></script>
	<script type="text/javascript" src ="js/jquery.coords.js"></script>
	<script type="text/javascript" src ="js/jquery.draggable.js"></script>
	<script type="text/javascript" src ="js/jquery.gridster.extras.js"></script>
	<script type="text/javascript" src ="js/jquery.gridster.js"></script>
</header>
<body>
<?php include './Login/register.html';?>
	<!-- Hauptbereich mit Men� und Widgets -->
	<div id=main>
		<div id=menu>
			<div id=addWidget>Widget hinzuf�gen</div>
		</div>
		<div id=content>
			<div class="gridster ready">
			    <ul style="list-style:none; height: 480px; width: 960px; position: relative;">
			        <li class="gs-w draggable" data-row="1" data-col="1" data-sizex="1" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			        <li class="gs-w draggable" data-row="2" data-col="1" data-sizex="1" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			        <li class="gs-w draggable" data-row="3" data-col="1" data-sizex="1" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			 
			        <li class="gs-w draggable" data-row="1" data-col="2" data-sizex="2" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			        <li class="gs-w draggable" data-row="2" data-col="2" data-sizex="2" data-sizey="2">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			 
			        <li class="gs-w draggable" data-row="1" data-col="4" data-sizex="1" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			        <li class="gs-w draggable" data-row="2" data-col="4" data-sizex="2" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			        <li class="gs-w draggable" data-row="3" data-col="4" data-sizex="1" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			 
			        <li class="gs-w draggable" data-row="1" data-col="5" data-sizex="1" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			        <li class="gs-w draggable" data-row="3" data-col="5" data-sizex="1" data-sizey="1">
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			 
			        <li class="gs-w draggable" data-row="1" data-col="6" data-sizex="1" data-sizey="1" >
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			        <li class="gs-w draggable" data-row="2" data-col="6" data-sizex="1" data-sizey="2" >
			        	<span class="gs-resize-handle gs-resize-handle-both"></span>
			        </li>
			    </ul>
>>>>>>> branch 'master' of https://bitbucket.org/tim-bauer/dashfeed
			</div>
		</div>
	</body>
</html>
