jQuery(function($) {
	
	/** ************ start: events. ************* */
	
	
	/** Register **/
	//Popups vorher verstecken
	$("div#registerpopup").hide();
	$("a.registerpopup").click(function() {
		loading(); // loading
		loadPopup(); // function show popup
		return false;
	});
	/* event for close the popup */
	$("div.close").hover(function() {
		$('span.ecs_tooltip').show();
	}, function() {
		$('span.ecs_tooltip').hide();
	});

	$("div.close").click(function() {
		disablePopup(); // function close pop up
	});

	$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is 'Ecs' in the keyboard
			disablePopup(); // function close pop up
		}
	});

	$("div#backgroundPopup").click(function() {
		disablePopup(); // function close pop up
	});

	$('a.livebox').click(function() {
		disablePopup();
		return false;
	});
	
	
	
	/** Widgets **/
	var gridster;
	
	//Gridster-Initialisierung
	$(function() { //DOM Ready
		$(".gridster ul").gridster({
	        widget_margins: [10, 10],
	        widget_base_dimensions: [140, 140]
	    });
		gridster = $(".gridster ul").gridster().data('gridster');
		gridster.resizable();
	});
	
	//Aktion, die beim Klick auf den Button addWidget ausgeführt wird
	$("div#addWidget").click(function() {
		//Erstellt ein Widget und return dieses als Objekt
		var widget = gridster.add_widget('<li class="gs-w draggable">' +
	        								'<div class="crossClose"></div>' +
	        								'<span class="gs-resize-handle gs-resize-handle-both"></span>' +
	        							'</li>', 2, 1);
	});

	//Entfernt das Widget beim Klick auf den X-Button
	$(".crossClose").click(function() {
		gridster.remove_widget($(this).parent());
	});
	
	/** ************ end: events. ************* */
	
	
	
	
	/** ************ start: functions. ************* */
	
	function loading() {
		$("div.loader").show();
	}
	function closeloading() {
		$("div.loader").fadeOut('normal');
	}

	var popupStatus = 0; // set value

	function loadPopup() {
		if (popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("div#registerpopup").fadeIn(0500); // fadein popup div
			$("div#backgroundPopup").css("opacity", "0.7"); // css opacity,
			$("div#backgroundPopup").fadeIn(0001);
			popupStatus = 1; // and set value to 1
		}
	}

	function disablePopup() {
		if (popupStatus == 1) { // if value is 1, close popup
			$("div#registerpopup").fadeOut("normal");
			$("div#backgroundPopup").fadeOut("normal");
			popupStatus = 0; // and set value to 0
		}
	}
	/** ************ end: functions. ************* */
}); // jQuery End
