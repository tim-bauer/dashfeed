function checkform() {

	var username = document.register.username;
	var usernamechange = false;
	var emailchange = false;
	var email = document.register.email;
	var passwort = document.register.passwort;
	var passwortchange = false;
	var passwort2 = document.register.passwort2;

	checkUsername();
	ValidateEmail();
	checkPasswort();

	function checkUsername() {
		if (username.value.length > 0) {
			document.getElementById("username").style.borderColor = "";
			// document.getElementById("errorBox").innerHTML = "";
			return checkAvailability();
		} else {
			document.getElementById("username").style.borderColor = "red";
			document.getElementById("errorBox").innerHTML = "Username muss länger als ein Zeichen sein";
			// alert('Username must be longer than one sign');
			return false;
		}
	}

	function ValidateEmail() {
		var mailformat = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
		if (email.value.match(mailformat)) {
			document.getElementById("email").style.borderColor = "";
			document.getElementById("errorBox").innerHTML = "";
			return true;
		} else {
			document.getElementById("email").style.borderColor = "red";
			document.getElementById("errorBox").innerHTML = "Keine gültige Emailadresse";
			// alert("You have entered an invalid email address!");
			return false;
		}
	}

	function checkPasswort() {
		if (passwort.value != passwort2.value) {
			// document.getElementById("passwort").style.borderColor="red";
			document.getElementById("passwort2").style.borderColor = "red";
			document.getElementById("errorBox").innerHTML = "Die Passwörter stimmen nicht überein";
			return false;
		} else if (passwort.value.length == 0 || passwort.value.length < 5
				|| passwort.value.length > 20) {
			document.getElementById("passwort").style.borderColor = "red";
			document.getElementById("errorBox").innerHTML = "Passwort muss zwischen 5 und 20 Stellen lang sein";
			return false;
		} else {
			document.getElementById("passwort").style.borderColor = "";
			document.getElementById("passwort2").style.borderColor = "";
			document.getElementById("errorBox").innerHTML = "";
			return true;
		}
	}
	function checkAvailability() {
		var usercheck = username.value;
		$
				.post(
						"Login/checkusername.php",
						{
							usercheck : usercheck
						},
						function(result) {
							// if the result is 1
							if (result == 1) {
								// show that the username is available
								document.getElementById("errorBox").innerHTML = "Username verfuegbar";
								return true;
							} else {
								// show that the username is NOT available
								document.getElementById("errorBox").innerHTML = "Username nicht verfuegbar";
								document.getElementById("username").style.borderColor = "red";
								return false;
							}
						});

	}

	if (!usernamechange) {
		document.getElementById("username").onkeyup = checkUsername;
		usernamechange = true;
	}
	if (!emailchange) {
		document.getElementById("email").onkeyup = ValidateEmail;
		emailchange = true;
	}
	if (!passwortchange) {
		document.getElementById("passwort").onkeyup = checkPasswort;
		document.getElementById("passwort2").onkeyup = checkPasswort;
		passwortchange = true;
	}

	return false;
}